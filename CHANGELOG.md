# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.2.0] - Tue April 21 - 3a27aeb

### Added
- deploy(0.2.0): put tags live
- feat(events): include tags with events

## [0.0.1] - Tue April 21 - 1fa8285

### Added
- deploy(0.0.1): inital mvp
- API MVP pre commit convention
- redis cache included