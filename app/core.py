import os
import redis
import json
from . import air_table_adapter


def get_events():
    # connect to redis
    host = os.environ.get('REDIS_HOST', 'localhost')
    redis_db = redis.StrictRedis(host=host, port=6379, db=0)

    # check cache for events and return
    cache_key = 'corona_cal:air_table_events'
    cached_json = redis_db.get(cache_key)
    if (bool(cached_json)):
        return cached_json

    # if no events in cache get them from api
    events = air_table_adapter.get_events()

    # save events into cache and return
    redis_db.set(cache_key, json.dumps(events))
    redis_db.expire(cache_key, 60) # 60 sec cache

    return events;
