from app import app

from . import core

cors_fix = { 'Access-Control-Allow-Origin': '*' }

@app.route('/corona-cal/v1/events', methods=['GET'])
def index():
    return (core.get_events(), cors_fix)

