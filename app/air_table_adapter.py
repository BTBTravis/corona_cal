import os
from functools import partial
import requests

api_key = os.environ['AIR_TABLE_API_KEY']
base_id = os.environ['AIR_TABLE_EVENT_BASE_ID']

def get_events():
    """Returns events from airtable"""
    tags = _get_tags()
    map_tags = partial(_map_tags, tags=tags)
    url = "https://api.airtable.com/v0/%s/events" % base_id
    r = requests.get(
        url,
        params={'view': 'Online Events'},
        headers={'authorization': "Bearer %s" % api_key}
    )
    j = r.json()
    events = list(map(lambda evt: {
        'id': evt['id'],
        'name': evt['fields']['name'],
        'start_date': evt['fields'].get('start_date', ''),
        'link': evt['fields']['link'],
        'tags': map_tags(evt['fields']['tags']) if 'tags' in evt['fields'] else []
        }, j['records']))

    tag_names = []
    for tag_id in tags:
        tag_names.append(tags[tag_id])

    return {
        'events': events,
        'tags': tag_names
    }

def _map_tags(tag_ids, tags):
    return list(map(lambda tag_id: tags[tag_id], tag_ids))


def _get_tags():
    url = "https://api.airtable.com/v0/%s/tags" % base_id
    r = requests.get(
        url,
        headers={'authorization': "Bearer %s" % api_key}
    )
    j = r.json()
    tags = {}
    for tag in j['records']:
        tags[tag['id']] = tag['fields']['Name'] 
    return tags
